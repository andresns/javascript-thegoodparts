// from previous chapter, augmenting a function p.32 (line 112)
Function.prototype.method = function (name, func) {
    if (!this.prototype[name]) { //check if the method exists
        this.prototype[name] = func;
        return this;
    }
};

// Inheritance

// Pseudoclassical

// if the "new" operator were a method instead an operator, it could have been implemented like this:
Function.method("new", function () {
    //Create a new object that inherits from the constructor's prototype
    var that = Object.create(this.prototype);

    //Invoke the constructor, binding -this- to the new object
    var other = this.apply(that, arguments);

    //If its return value isnt an object, substitute the new object.

    return (typeof other === "object" && other) || that; //The || operator can be used to fill in default values
});


//We can define a constructor and augment its prototype
var Mammal = function (name) {
    this.name = name;
};

Mammal.prototype.get_name = function () {
    return this.name;
};

Mammal.prototype.says = function () {
    return this.saying || "";
};

//Now, we can make an instance

var myMammal = new Mammal("Herb the Mammal");
var name = myMammal.get_name();

console.log(name);

//We can make another pseudoclass that inherits from Mammal by defining its constructor function and replacing its prototype with an instance of Mammal

var Cat = function () {
    this.name = name;
    this.saying = "meow";
};

//replace Cat.prototype with a new instance of Mammal

Cat.prototype = new Mammal();

//Augment the new prototype with purr and get_name methods

Cat.prototype.purr = function (n) {
    var i, s = "";
    for (i = 0; i < n; i++) {
        if (s) {
            s += "-";
        }
        s += "r";
    }
    return s;
};

Cat.prototype.get_name = function () {
    return this.says() + " " + this.name + " " + this.says();
};

var myCat = new Cat("Henrietta");
var says = myCat.says(); //"meow"
var purr = myCat.purr(5); //r-r-r-r-r
var name = myCat.get_name(); //"meow Henrietta meow"

//Defining an inherits method

Function.method("inherits", function (Parent) {
    this.prototype = new Parent();
    return this;
});

//we can use cascade style because "inherits" and "method" methods return "this"
var Cat = function (name) {
    this.name = name;
    this.saying = "meow";
}.inherits(Mammal)
    .method("purr", function (n) {
        var i, s = "";
        for (i = 0; i < n; i++) {
            if (s) {
                s += "-";
            }
            s += "r";
        }
        return s;
    }).method("get_name", function () {
        return this.says() + " " + this.name + " " + this.says();
    })

//Prototypal

//Define an object
var myMammal = {
    name: "Herb the Mammal",
    get_name: function () {
        return this.name;
    },
    says: function () {
        return this.saying || "";
    }
};

//using the create method
var myCat = Object.create(myMammal);
myCat.name = "Henrietta";
myCat.saying = "meow";
myCat.purr = function (n) {
    var i, s = "";
    for (i = 0; i < n; i++) {
        if (s) {
            s += "-";
        }
        s += "r";
    }
    return s;
};

myCat.get_name = function () {
    return this.says() + " " + this.name + " " + this.says();
};


//Functional

var mammal = function (spec) {
    var that = {}

    that.get_name = function () {
        return spec.name;
    };
    that.says = function () {
        return spec.saying || "";
    };

    return that;
};

var myMammal = mammal({ name: "Herb" });

var cat = function (spec) {
    spec.saying = spec.saying || "meow";
    var that = mammal(spec);
    that.purr = function (n) {
        var i, s = "";
        for (i = 0; i < n; i++) {
            if (s) {
                s += "-";
            }
            s += "r";
        }
        return s;
    };
    that.get_name = function () {
        return that.says() + " " + spec.name + " " + that.says();
    };
    return that;
};

var myCat = cat({ name: "Henrietta" });

//The functional pattern also gives us a way to deal with super methods

Object.create("superior", function (name) {
    var that = this,
        method = that[name];
    return function () {
        return method.apply(that, arguments);
    };
});

var coolcat = function (spec) {
    var that = cat(spec),
        super_get_name = that.superior("get_name");
    that.get_name = function () {
        return "like" + super_get_name() + " baby";
    };
    return that;
};

var myCoolCat = coolcat({ name: "Bix" });
var name = myCoolCat.get_name(); //"like meow Bix meow baby"

//Parts

var eventuality = function (that) {
    var registry = {};
    that.fire = function (event) {
        //Fire an event on an object. The event can be either a string containing the name of the event or an object containning a type property containing the name of the event. Handlers registered by the "on" method that match the event name will be invoked.
        var array,
            func,
            handler,
            i,
            type = typeof event === "string" ? event : event.type;

        //if an array of handler exist for this event, then loop through it and execute the handlers in order.
        if (registry.hasOwnProperty(type)) {
            array = registry[type];
            for (i = 0; i < array.lenght; i++) {
                handler = array[i];
                
                //A handler record contains a method and an optional array of parameters. if the method is a name, look up the function.

                func = handler.method;
                if(typeof func === "string"){
                    func = this[func];
                }

                //invoke a handler. if the record contained parameters, then pass them. Otherwise, pass the event object.

                func.apply(this, handler.parameters || [event]);
            }
        }
        return this;
    };

    that.on = function (type, method, parameters){
        //Register an event. MAke a handler record. Put it in a hander array, making one if it doesn't yet exist for this type.

        var handler = {
            method: method,
            parameters: parameters
        };
        if(registry.hasOwnProperty(type)){
            registry[type].push(handler);
        }else{
            registry[type] = [handler];
        }
        return this;
    };
    return that;
};