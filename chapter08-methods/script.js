/* Array Methods */

// concat
var a = ["a", "b", "c"];
var b = ["x", "y", "z"];
var c = a.concat(b, true);

console.log(c); //[ 'a', 'b', 'c', 'x', 'y', 'z', true ]

//join
var a = ["a", "b", "c"];
a.push("d");
console.log(a.join("-")); //a-b-c-d   default separator is ,

//pop
var a = ["a", "b", "c"];
var c = a.pop();
console.log(a);
console.log(c);

//push
var a = ["a", "b", "c"];
var b = ["x", "y", "z"];
console.log(a.push(b, true)); //returns the new length (5)

//reverse
var a = ["a", "b", "c"];
console.log(a.reverse()); //[ 'c', 'b', 'a' ]

//shift
var a = ["a", "b", "c"];
console.log(a.shift()); //shift is slower than pop

//slice
var a = ["a", "b", "c"];
console.log(a.slice(0, 1));
console.log(a.slice(1));
console.log(a.slice(1, 2));

//sort
//sort strings in an array (avoid using with numbers)
var n = [4, 8, 15, 16, 23, 42];
console.log(n.sort()); //[ 15, 16, 23, 4, 42, 8 ] converts to string and then sorts the elements

//to make it work with numbers we need to pass a function. The functionmust return a negative number if the first parameter should go first, 0 if they are equal and a positive number if its greater.
//by doing this, the sort function no longer sort strings.
console.log(n.sort(function (a, b) {
    return a - b;
}));

//splice
var a = ["a", "b", "c"];
console.log(a.splice(1,1,"ache", "bug")); //[ 'b' ]
console.log(a); //[ 'a', 'ache', 'bug', 'c' ]

//unshift
var a = ["a", "b", "c"];
console.log(a.unshift("?","@")); //returns 5 (new length)

/* Function Methods */

//apply
//invokes a function, passing in the object that will be bound to this and an optional array of arguments.
//see page 84


/* Number Methods */

//toExponential
//converts a number to string in the exponential form. It has an optional parameter that controls de number of decimals

console.log(Math.PI.toExponential());
console.log(Math.PI.toExponential(0));
console.log(Math.PI.toExponential(2));
console.log(Math.PI.toExponential(10));

//toFixed
//converts a number to string in the decimal form. parameter for decimals
console.log(Math.PI.toFixed());
console.log(Math.PI.toFixed(2));
console.log(Math.PI.toFixed(7));
console.log(Math.PI.toFixed(10));

//toPrecision
//converts a number to string in the decimal form. parameter for number of precision (include the integer part)
console.log(Math.PI.toPrecision());
console.log(Math.PI.toPrecision(2));
console.log(Math.PI.toPrecision(7));
console.log(Math.PI.toPrecision(10));

//toString
//converts a number to string. 
// Optional parameter. Which base to use for representing a numeric value. Must be an integer between 2 and 36.

// 2 - The number will show as a binary value
// 8 - The number will show as an octal value
// 16 - The number will show as an hexadecimal value

console.log(Math.PI.toString());
console.log(Math.PI.toString(2));
console.log(Math.PI.toString(8));
console.log(Math.PI.toString(16));


/* RegExp Methods */
//See page 86


/* String Methods */

//charAt
var name = "Curly";
console.log(name.charAt(0)); //C

//charCodeAt
//like charAt, but returns an integer representation of the character
var name = "Curly";
console.log(name.charCodeAt(0)); //67

//concat
console.log("C".concat("a", "t")); //Cat

//indexOf
var text = "Mississipi";
console.log(text.indexOf("ss")); //2
console.log(text.indexOf("ss",3)); //5
console.log(text.indexOf("ss",6)); //-1  (not found)

//lastIndexOf
//like indexOF, but starting from the end
var text = "Mississipi";
console.log(text.lastIndexOf("ss")); //5
console.log(text.lastIndexOf("ss",3)); //2
console.log(text.lastIndexOf("ss",6)); //5

//localeCompare
//compares 2 strings, if the string is less than the string in the argument, the result is negative, positive if greater and 0 if they are equal
//Determines whether two strings are equivalent in the current or specified locale.
console.log("text".localeCompare("text")); //0
console.log("text".localeCompare("test")); // 1
console.log("text".localeCompare("texto")); //-1

//match
//matches a string and a regular expression
//see page 89

//replace
console.log("mother_in_law".replace("_", "-")); //mother-in_law
//to replace all the characters found we can use a regular expression with the g flag. (see page 90)

//search
//is like indexOf, except that it takes a regular expression object instead of a string
console.log('and in it he says "Any damn fool could'.search(/["']/)); //18

//slice
// makes a new string by copying a portion of another string
var text = 'and in it he says "Any damn fool could';
console.log(text.slice(18)); //"Any damn fool could
console.log(text.slice(0,3)); //and
console.log(text.slice(-5)); //could
console.log(text.slice(19,32)); //Any damn fool

//split
// creates an array of strings (second parameter is limit)
var digits = "0123456789";
console.log(digits.split("", 5)); //[ '0', '1', '2', '3', '4' ]
var ip = "192.168.1.0";
console.log(ip.split(".")); //[ '192', '168', '1', '0' ]
//regular expressions examples in page 92

//fromCharCode
// produces a string from a series of numbers
console.log(String.fromCharCode(67,97,116)); //Cat