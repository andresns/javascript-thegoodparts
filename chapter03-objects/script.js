//Objects

var stooge = {
    "first-name": "Jerome",
    "last-name": "Howard",
}

//We can set a default value using the || operator
var middle = stooge["middle-name"] || "(none)";

var flight = {
    airline: "Oceanic",
    number: 815,
    departure: {
        IATA: "SYD",
        time: "2004-09-22 14:55",
        city: "Sydney"
    },
    arrival: {
        IATA: "LAX",
        time: "2004-09-23 14:42",
        city: "Los Angeles"
    }
};

/*

flight.equipment //undefined
flight.equipment.model //throw TypeError
flight.equipment && flight.equipment.model //undefined

*/

// Objects are passed around reference. They are never copied.
var x = stooge;
x.nickname = "Curly";
var nick = stooge.nickname; //nick is curly, because x and stooge are references to the same object

var a = {}, b = {}, c = {}; //a, b and c each refer to a different empty object.
a = b = c = {}; //a, b and c all refer to the same empty object.

//Create object with stooge as a prototype
var another_stooge = Object.create(stooge);

another_stooge["first-name"] = "Harry";
another_stooge["middle-name"] = "Moses";
another_stooge.nickname = "Moe";

//if we add a new property to an object, all the object created based on that object, will have that property.
stooge.profesion = "actor";
another_stooge.profesion    //actor

//Deleting a property from an object will reveal the property from the prototype, if ti has one.
another_stooge.nickname //"Moe"
delete another_stooge.nickname;
another_stooge.nickname // "Curly"